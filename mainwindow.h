/*
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <regex>
#include <float.h>

#include <QtGui>
#include <QMainWindow>
#include <QDialog>
#include <QListView>
#include <QStandardItemModel>
#include <QStringList>
#include <QComboBox>

#include "export_pdf_dialog.h"
#include "export_image_dialog.h"
#include "settingsdialog.h"
#include "modelwrapper.h"
#include "plotbuilder.h"

#define DYNAMIC_PADDING_PARAMETER 900
#define PEN_WIDTH 3
#define PADDING_EVENT_VALUE DBL_MIN

#define INDEX_PLOT_TYPE 0
#define INDEX_PLOT_MODULE 1

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:

    QCustomPlot* SelectedPlot();

    Ui::MainWindow *ui;
    SettingsDialog *settingsDialog;
    ExportPdfDialog *exportPdfDialog;
    ExportImageDialog *exportImageDialog;
    bbque::ModelWrapper *modelWrapper;
    PlotBuilder *plotBuilder;
    int currentRowModule;
    int currentRowType;

private slots:

    /**
    * @brief ApplyFiltersModule
    * Handler called when the user changes a filter in the Modules
    * tab. The event list and the plot are updated accordingly
    */
    void ApplyFiltersModule();

    /**
    * @brief ApplyFiltersType
    * Handler called when the user changes a filter in the Types
    * tab. The event list and the plot are updated accordingly
    */
    void ApplyFiltersType();

    /**
    * @brief FillUiWithData
    * Handler called when the user closes the SettingsDialog.
    * Used to update UI (plot and filters) with the selected modules
    * @param the plotHeight received from SettingsDialog signal
    * @param hidden flag specifying whether the mainwindow must
    * be hidden or not
    */
    void FillUiWithData(int plotHeight, bool hidden);

    void OpenSettings(int startup);
    void OpenExportPdfDialog();
    void OpenExportImageDialog();

    /**
    * @brief OnMouseMoveGraph
    * Handler called at each movement of the mouse hover the plot.
    * It is necessary for detecting the relative position of the
    * mouse for understanding which is the closest event for
    * showing the tooltip
    */
    void OnMouseMoveGraph(QMouseEvent*);

    /**
    * @brief EventSelectedFromModuleTable
    * Handler called at each movement the user selects a different
    * event in the event list in the Modules tab. It is necessary for plotting the
    * vertical line in the graphs for highlighting the selected event
    */
    void EventSelectedFromModuleTable(QModelIndex);

    /**
    * @brief EventSelectedFromTypeTable
    * Handler called at each movement the user selects a different
    * event in the event list in the Types tab. It is necessary for plotting the
    * vertical line in the graphs for highlighting the selected event
    */
    void EventSelectedFromTypeTable(QModelIndex);

    void Exit();
};

#endif // MAINWINDOW_H
