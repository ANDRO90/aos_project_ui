#-------------------------------------------------
#
# Project created by QtCreator 2015-10-01T23:08:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET   = bbque-event-viewer
TEMPLATE = app

INCLUDEPATH += $$BUILD_DIR/include

LIBS += -L$$BUILD_DIR/lib -L$$BUILD_DIR/lib/bbque -L/usr/lib/x86_64-linux-gnu $$BUILD_DIR/lib/bbque/libbbque_em.a $$BUILD_DIR/lib/bbque/libbbque_logger.a
LIBS += -lboost_program_options -lboost_system -lboost_filesystem -lboost_serialization -llog4cpp

QMAKE_RPATHDIR += $$BUILD_DIR/lib

DESTDIR = $$BUILD_DIR/bin
OBJECTS_DIR = ./build-$$BUILD_TYPE

CONFIG += c++11

BUILD_TYPE = $$BUILD_TYPE

equals(BUILD_TYPE,Debug) {
    DEFINES += DEBUG
    QMAKE_CXXFLAGS += -g
}

SOURCES +=\
    model.cc \
    export_image_dialog.cc \
    export_pdf_dialog.cc \
    main.cc \
    mainwindow.cc \
    plotbuilder.cc \
    qcpdocumentobject.cc \
    qcustomplot.cc \
    settingsdialog.cc \
    modelwrapper.cc

HEADERS  += mainwindow.h \
    model.h \
    settingsdialog.h \
    qcustomplot.h \
    qcpdocumentobject.h \
    export_pdf_dialog.h \
    export_image_dialog.h \
    plotbuilder.h \
    modelwrapper.h

FORMS    += mainwindow.ui \
    settingsdialog.ui \
    export_pdf_dialog.ui


