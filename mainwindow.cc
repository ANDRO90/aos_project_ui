#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    modelWrapper = bbque::ModelWrapper::GetInstance();
    settingsDialog = new SettingsDialog(this);
    exportPdfDialog = new ExportPdfDialog(this);
    plotBuilder = new PlotBuilder();

    QSignalMapper* signalMapper = new QSignalMapper(this);
    connect(ui->actionSettings, SIGNAL(triggered()), signalMapper, SLOT(map()));
    signalMapper->setMapping(ui->actionSettings, 0); //not startup
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OpenSettings(int)));

    connect(ui->action_jpg, SIGNAL(triggered(bool)),
            this, SLOT(OpenExportImageDialog()));
    connect(ui->action_png, SIGNAL(triggered(bool)),
            this, SLOT(OpenExportImageDialog()));
    connect(ui->actionExport_as_pdf, SIGNAL(triggered(bool)),
            this, SLOT(OpenExportPdfDialog()));
    connect(ui->actionExit_2, SIGNAL(triggered(bool)), this, SLOT(Exit()));
    connect(settingsDialog, SIGNAL(SettingsAccepted(int, bool)),
            this, SLOT(FillUiWithData(int, bool)));
    OpenSettings(1); //startup

    connect(ui->tableView_module,
            SIGNAL(clicked(QModelIndex)),
            this, SLOT(EventSelectedFromModuleTable(QModelIndex)));

    connect(ui->tableView_type,
            SIGNAL(clicked(QModelIndex)),
            this, SLOT(EventSelectedFromTypeTable(QModelIndex)));

    //link handlers to timestamp filters
    connect(ui->lineEdit_module_from, SIGNAL(editingFinished()),
            this, SLOT(ApplyFiltersModule()));

    connect(ui->lineEdit_module_to, SIGNAL(editingFinished()),
            this, SLOT(ApplyFiltersModule()));

    connect(ui->lineEdit_type_from, SIGNAL(editingFinished()),
            this, SLOT(ApplyFiltersType()));

    connect(ui->lineEdit_type_to, SIGNAL(editingFinished()),
            this, SLOT(ApplyFiltersType()));

    //link handlers to filters
    connect(ui->comboBox_module_module,
            SIGNAL(currentIndexChanged(const QString&)),
            this, SLOT(ApplyFiltersModule()));

    connect(ui->comboBox_module_application,
            SIGNAL(currentIndexChanged(const QString&)),
            this, SLOT(ApplyFiltersModule()));

    connect(ui->comboBox_module_resource,
            SIGNAL(currentIndexChanged(const QString&)),
            this, SLOT(ApplyFiltersModule()));

    connect(ui->comboBox_module_type,
            SIGNAL(currentIndexChanged(const QString&)),
            this, SLOT(ApplyFiltersModule()));

    connect(ui->comboBox_type_module,
            SIGNAL(currentIndexChanged(const QString&)),
            this, SLOT(ApplyFiltersType()));

    connect(ui->comboBox_type_application,
            SIGNAL(currentIndexChanged(const QString&)),
            this, SLOT(ApplyFiltersType()));

    connect(ui->comboBox_type_resource,
            SIGNAL(currentIndexChanged(const QString&)),
            this, SLOT(ApplyFiltersType()));

    connect(ui->comboBox_type_type,
            SIGNAL(currentIndexChanged(const QString&)),
            this, SLOT(ApplyFiltersType()));

    //link handlers to plots
    connect(ui->plot_module, SIGNAL(mouseMove(QMouseEvent*)),
            this, SLOT(OnMouseMoveGraph(QMouseEvent*)));
    connect(ui->plot_type, SIGNAL(mouseMove(QMouseEvent*)),
            this, SLOT(OnMouseMoveGraph(QMouseEvent*)));

    ui->tabWidget->setCurrentIndex(INDEX_PLOT_TYPE);
}
MainWindow::~MainWindow()
{
    delete ui;
}

QCustomPlot* MainWindow::SelectedPlot()
{
    if(ui->tabWidget->currentIndex() == INDEX_PLOT_TYPE)
        return ui->plot_type;
    else
        return ui->plot_module;
}

void MainWindow::Exit()
{
    this->close();
}

void MainWindow::OpenSettings(int startup = 0)
{
    if(startup == 0 && !settingsDialog->IsSettingsStateAccepted())
    {
        settingsDialog->RestoreSettingsState();
    }

    settingsDialog->show();
}

void MainWindow::OpenExportPdfDialog()
{
    exportPdfDialog->Open(SelectedPlot());
}

void MainWindow::OpenExportImageDialog()
{
    if(sender()->objectName().contains("jpg"))
        exportImageDialog = new ExportImageDialog(
            SelectedPlot(), ExportImageDialog::ExportMode::JPG);
    else
    if(sender()->objectName().contains("png"))
        exportImageDialog = new ExportImageDialog(
            SelectedPlot(), ExportImageDialog::ExportMode::PNG);
}

void MainWindow::FillUiWithData(int plotHeight, bool hidden)
{
    //clear row selection
    currentRowModule = -1;
    currentRowType = -1;

    //plotHeight received from the SettingsDialog signal
    bool oldState = false;
    //populate filters Modules tab
    oldState = ui->comboBox_module_module->blockSignals(true);
    ui->comboBox_module_module->clear();
    ui->comboBox_module_module->addItems(
                modelWrapper->GetDynamicModelClean()->GetDistinctModules());
    ui->comboBox_module_module->blockSignals(oldState);

    oldState = ui->comboBox_module_application->blockSignals(true);
    ui->comboBox_module_application->clear();
    ui->comboBox_module_application->addItems(
                modelWrapper->GetDynamicModelClean()->GetDistinctApplications());
    ui->comboBox_module_application->blockSignals(oldState);

    oldState = ui->comboBox_module_resource->blockSignals(true);
    ui->comboBox_module_resource->clear();
    ui->comboBox_module_resource->addItems(
                modelWrapper->GetDynamicModelClean()->GetDistinctResources());
    ui->comboBox_module_resource->blockSignals(oldState);

    oldState = ui->comboBox_module_type->blockSignals(true);
    ui->comboBox_module_type->clear();
    ui->comboBox_module_type->addItems(
                modelWrapper->GetDynamicModelClean()->GetDistinctTypes());
    ui->comboBox_module_type->blockSignals(oldState);

    //populate filters Types tab
    oldState = ui->comboBox_type_module->blockSignals(true);
    ui->comboBox_type_module->clear();
    ui->comboBox_type_module->addItems(
                modelWrapper->GetDynamicModelClean()->GetDistinctModules());
    ui->comboBox_type_module->blockSignals(oldState);

    oldState = ui->comboBox_type_application->blockSignals(true);
    ui->comboBox_type_application->clear();
    ui->comboBox_type_application->addItems(
                modelWrapper->GetDynamicModelClean()->GetDistinctApplications());
    ui->comboBox_type_application->blockSignals(oldState);

    oldState = ui->comboBox_type_resource->blockSignals(true);
    ui->comboBox_type_resource->clear();
    ui->comboBox_type_resource->addItems(
                modelWrapper->GetDynamicModelClean()->GetDistinctResources());
    ui->comboBox_type_resource->blockSignals(oldState);

    oldState = ui->comboBox_type_type->blockSignals(true);
    ui->comboBox_type_type->clear();
    ui->comboBox_type_type->addItems(
                modelWrapper->GetDynamicModelClean()->GetDistinctTypes());
    ui->comboBox_type_type->blockSignals(oldState);

    //link dynamic_model_modules to the TableView in Modules tab
    QSortFilterProxyModel *proxyModel_module = new QSortFilterProxyModel;
    proxyModel_module->setSourceModel(
                modelWrapper->GetDynamicModelFilteredModule());
    ui->tableView_module->setModel(proxyModel_module);

    //link dynamic_model_types to the TableView in Types tab
    QSortFilterProxyModel *proxyModel_type = new QSortFilterProxyModel;
    proxyModel_type->setSourceModel(modelWrapper->GetDynamicModelFilteredType());
    ui->tableView_type->setModel(proxyModel_type);

    //adjust horizontal spacing in the TableView in Modules tab
    for (int c = 0; c < ui->tableView_module->horizontalHeader()->count(); ++c)
    {
        ui->tableView_module->horizontalHeader()->setSectionResizeMode(c,
                                                        QHeaderView::Stretch);
    }

    //adjust horizontal spacing in the TableView in Types tab
    for (int c = 0; c < ui->tableView_type->horizontalHeader()->count(); ++c)
    {
        ui->tableView_type->horizontalHeader()->setSectionResizeMode(c,
                                                        QHeaderView::Stretch);
    }

    ui->tableView_module->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tableView_type->setSelectionBehavior(QAbstractItemView::SelectRows);

    ui->tableView_module->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tableView_type->setSelectionMode(QAbstractItemView::SingleSelection);

    //populate timestamp filters
    ui->lineEdit_module_from->setText(QString("%1").arg(
        modelWrapper->GetDynamicModelFilteredModule()->GetLowestTimestamp(),
                                          0, 'f', 0));

    ui->lineEdit_module_to->setText(QString("%1").arg(
        modelWrapper->GetDynamicModelFilteredModule()->GetHighestTimestamp(),
                                        0, 'f', 0));

    ui->lineEdit_type_from->setText(QString("%1").arg(
        modelWrapper->GetDynamicModelFilteredType()->GetLowestTimestamp(),
                                        0, 'f', 0));

    ui->lineEdit_type_to->setText(QString("%1").arg(
        modelWrapper->GetDynamicModelFilteredType()->GetHighestTimestamp(),
                                      0, 'f', 0));

    plotBuilder->BuildPlotModules(ui->plot_module, plotHeight);
    plotBuilder->BuildPlotTypes(ui->plot_type, plotHeight);

    //workaround for the QMessageBox bug...showing and instantly
    //hiding the mainwindow avoids the program to stop.
    this->showMaximized();
    ui->menuBar->show();
    if(hidden)
    {
        this->hide();
    }

    //resize splitter...set default size as 1/3 for plot and 2/3 for events list
    QList<int> list = QList<int>();
    QSize splitter_size = ui->plot_module->size();
    list.append(splitter_size.height()/3);
    list.append(splitter_size.height()/3*2);

    ui->splitter_1->setSizes(list);
    ui->splitter_2->setSizes(list);

    ui->plot_module->setAttribute(Qt::WA_NoMousePropagation, false);
    ui->plot_type->setAttribute(Qt::WA_NoMousePropagation, false);

    ui->plot_module->replot();
    ui->plot_type->replot();
}
void MainWindow::ApplyFiltersModule()
{
    //get filters value
    QString m = ui->comboBox_module_module->currentText();
    QString a = ui->comboBox_module_application->currentText();
    QString r = ui->comboBox_module_resource->currentText();
    QString t = ui->comboBox_module_type->currentText();
    QString from = ui->lineEdit_module_from->text();
    QString to = ui->lineEdit_module_to->text();

    //update model module
    modelWrapper->ApplyFilters(
            modelWrapper->GetDynamicModelFilteredModule(), m, a, r, t, from, to);

    //update timestamp extremes
    modelWrapper->GetDynamicModelFilteredModule()
            ->SetLowestTimestamp(from.toDouble());
    modelWrapper->GetDynamicModelFilteredModule()
            ->SetHighestTimestamp(to.toDouble());

    //link updated dynamic_model_modules to the TableView in Modules tab
    QSortFilterProxyModel *proxyModel_module = new QSortFilterProxyModel;
    proxyModel_module->setSourceModel(
                modelWrapper->GetDynamicModelFilteredModule());
    ui->tableView_module->setModel(proxyModel_module);

    //have to block signals because at this point there is a
    //signal attached to the plot and therefore it would fire
    bool oldState = ui->plot_module->blockSignals(true);
    plotBuilder->BuildPlotModules(ui->plot_module);
    ui->plot_module->blockSignals(oldState);

    ui->plot_module->replot();
}

void MainWindow::ApplyFiltersType()
{
    //get filters value
    QString m = ui->comboBox_type_module->currentText();
    QString a = ui->comboBox_type_application->currentText();
    QString r = ui->comboBox_type_resource->currentText();
    QString t = ui->comboBox_type_type->currentText();
    QString from = ui->lineEdit_type_from->text();
    QString to = ui->lineEdit_type_to->text();

    //update model type
    modelWrapper->ApplyFilters(modelWrapper->GetDynamicModelFilteredType(),
                             m, a, r, t, from, to);

    //update timestamp extremes
    modelWrapper->GetDynamicModelFilteredType()
            ->SetLowestTimestamp(from.toDouble());
    modelWrapper->GetDynamicModelFilteredType()
            ->SetHighestTimestamp(to.toDouble());

    //link updated dynamic_model_types to the TableView in Types tab
    QSortFilterProxyModel *proxyModel_module = new QSortFilterProxyModel;
    proxyModel_module->setSourceModel(
                modelWrapper->GetDynamicModelFilteredType());
    ui->tableView_type->setModel(proxyModel_module);

    //have to block signals because at this point there is a
    //signal attached to the plot and therefore it would fire
    bool oldState = ui->plot_type->blockSignals(true);
    plotBuilder->BuildPlotTypes(ui->plot_type);
    ui->plot_type->blockSignals(oldState);

    ui->plot_type->replot();
}

void MainWindow::OnMouseMoveGraph(QMouseEvent* event)
{
    QCPAbstractPlottable *plottable;

    //find which tab is selected
    int tab = ui->tabWidget->currentIndex();

    //get mouse position
    qreal ex = event->localPos().x();

    //retrieve graph under the mouse
    if(tab == INDEX_PLOT_TYPE)
    {        
        plottable = ui->plot_type->plottableAt(event->localPos());
    }
    else
    {
        plottable = ui->plot_module->plottableAt(event->localPos());
    }

    if(plottable)
    {

        QCPGraph *graph = qobject_cast<QCPGraph*>(plottable);

        //retrieve timestamp and value under the mouse
        double x = graph->keyAxis()->pixelToCoord(ex);

        if(graph)
        {
            double key = 0;
            double value = 0;

            bool ok = false;
            double m = std::numeric_limits<double>::max();

            //algorithm for finding the closest event plotted in
            //the graph under the mouse
            foreach(QCPData data, graph->data()->values())
            {
                double d = qAbs(x - data.key);

                if(d < m)
                {
                    key = data.key;
                    value = data.value;

                    ok = true;
                    m = d;
                }
            }

            if(ok) //event found
            {
                if(tab == INDEX_PLOT_TYPE)
                {
                    if(value != DBL_MIN) //avoid fake padding events
                    {
                        QToolTip::hideText();

                        //get event info
                        bbque::Event e =
                            modelWrapper->GetDynamicModelFilteredType()
                                ->GetEventByTimestampAndType(
                                    QString("%1").arg(key, 0, 'f', 0),
                                    graph->name());

                        if(e.IsValid())
                        {
                            //build tooltip
                            QToolTip::showText(event->globalPos(),
                            tr("<table>"
                                    "<tr>"
                                        "<td>Timestamp:</td>" "<td>%1</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Module:</td>" "<td>%2</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Application:</td>" "<td>%3</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Resource:</td>" "<td>%4</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Type:</td>" "<td>%5</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Value:</td>" "<td>%6</td>"
                                    "</tr>"
                               "</table>").
                               arg(key, 0, 'f', 0).
                               arg(QString(e.GetModule().c_str())).
                               arg(QString(e.GetApplication().c_str())).
                               arg(QString(e.GetResource().c_str())).
                               arg(graph->name().isEmpty() ? "..." :
                                                             graph->name()).
                               arg(value),
                               ui->plot_type, ui->plot_type->rect());
                        }
                    }
                }
                else //plot modules
                {
                    if(value != DBL_MIN) //avoid fake padding events
                    {
                        QToolTip::hideText();

                        //get event info
                        bbque::Event e =
                            modelWrapper->GetDynamicModelFilteredModule()
                                ->GetEventByTimestampAndModule(
                                    QString("%1").arg(key, 0, 'f', 0),
                                    graph->name());

                        if(e.IsValid())
                        {
                            //build tooltip
                            QToolTip::showText(event->globalPos(),
                            tr("<table>"
                                    "<tr>"
                                        "<td>Timestamp:</td>" "<td>%1</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Module:</td>" "<td>%2</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Application:</td>" "<td>%3</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Resource:</td>" "<td>%4</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Type:</td>" "<td>%5</td>"
                                    "</tr>"
                                    "<tr>"
                                        "<td>Value:</td>" "<td>%6</td>"
                                    "</tr>"
                               "</table>").
                               arg(key, 0, 'f', 0).
                               arg(QString(e.GetModule().c_str())).
                               arg(QString(e.GetApplication().c_str())).
                               arg(QString(e.GetResource().c_str())).
                               arg(graph->name().isEmpty() ? "..." :
                                                             graph->name()).
                               arg(value),
                               ui->plot_module, ui->plot_module->rect());
                        }
                    }
                }
            }
        }
    }

}
void MainWindow::EventSelectedFromModuleTable(QModelIndex current)
{
    //retrieve selected event
    int row = current.row();

    //get event data
    double timestamp = current.sibling(row, 1).data().toDouble();
    QString module = current.sibling(row, 2).data().toString();
    QString resource = current.sibling(row, 3).data().toString();
    QString application = current.sibling(row, 4).data().toString();
    QString type = current.sibling(row, 5).data().toString();

    //remove previous lines
    QCustomPlot *q = ui->plot_module;
    foreach(QCPLayerable *i, q->currentLayer()->children())
    {
        if(strcmp(i->metaObject()->className(),
                  "QCPItemStraightLine") == 0)
        {
            i->setLayer(0);
        }
    }

    if(row != currentRowModule) //row selected
    {
        currentRowModule = row;

        //add new line to the graphs in which the event is present
        foreach (QCPAxisRect *rect, ui->plot_module->axisRects())
        {
            foreach(QCPGraph *g, rect->graphs())
            {
                if(g->name() == module)
                {
                    QCPItemStraightLine* l =
                            new QCPItemStraightLine(ui->plot_module);
                    l->setPen(QPen(QColor("red"), 0));
                    l->point1->setType(QCPItemPosition::ptPlotCoords);
                    l->point2->setType(QCPItemPosition::ptPlotCoords);
                    l->point1->setAxes(g->keyAxis(), g->valueAxis());
                    l->point2->setAxes(g->keyAxis(), g->valueAxis());
                    l->point1->setCoords(timestamp, 0.0);
                    l->point2->setCoords(timestamp,
                        modelWrapper->GetDynamicModelFilteredModule()
                                         ->GetHighestValue());
                    l->setClipAxisRect(rect);

                    break;
                }
            }
        }
    }
    else //row deselected
    {
        currentRowModule = -1;
        ui->tableView_module->selectionModel()->select(current,
            QItemSelectionModel::Deselect | QItemSelectionModel::Rows);
    }

    ui->plot_module->replot();
}

void MainWindow::EventSelectedFromTypeTable(QModelIndex current)
{
    //retrieve selected event
    int row = current.row();

    //get event data
    double timestamp = current.sibling(row, 1).data().toDouble();
    QString module = current.sibling(row, 2).data().toString();
    QString resource = current.sibling(row, 3).data().toString();
    QString application = current.sibling(row, 4).data().toString();
    QString type = current.sibling(row, 5).data().toString();

    //remove previous lines
    QCustomPlot *q = ui->plot_type;
    foreach(QCPLayerable *i, q->currentLayer()->children())
    {
        if(strcmp(i->metaObject()->className(), "QCPItemStraightLine") == 0)
        {
            i->setLayer(0);
        }
    }

    if(row != currentRowType) //row selected
    {
        currentRowType = row;

        //add new line to the graphs in which the event is present
        foreach (QCPAxisRect *rect, ui->plot_type->axisRects())
        {
            foreach(QCPGraph *g, rect->graphs())
            {
                if(g->name() == type)
                {
                    QCPItemStraightLine* l = new QCPItemStraightLine(ui->plot_type);
                    l->setPen(QPen(QColor("red"), 0));
                    l->point1->setType(QCPItemPosition::ptPlotCoords);
                    l->point2->setType(QCPItemPosition::ptPlotCoords);
                    l->point1->setAxes(g->keyAxis(), g->valueAxis());
                    l->point2->setAxes(g->keyAxis(), g->valueAxis());
                    l->point1->setCoords(timestamp, 0.0);
                    l->point2->setCoords(timestamp,
                        modelWrapper->GetDynamicModelFilteredType()
                                         ->GetHighestValue());
                    l->setClipAxisRect(rect);

                    break;
                }
            }
        }
    }
    else //row deselected
    {
        currentRowType = -1;
        ui->tableView_type->selectionModel()->select(current,
            QItemSelectionModel::Deselect | QItemSelectionModel::Rows);
    }

    ui->plot_type->replot();
}
