/*
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXPORTIMAGEDIALOG_H
#define EXPORTIMAGEDIALOG_H

#include "qdialog.h"
#include "qcustomplot.h"

#define FILENAME_DEFAULT "plot"
#define FILTER_ANY_FILE "Any files (*)"
#define JPG_IMAGE "JPG"
#define PNG_IMAGE "PNG"

class ExportImageDialog: public QDialog
{
    Q_OBJECT

public:
    /**
    * @brief ExportMode
    * Enum used to list all the export format for images.
    */
    enum ExportMode{ JPG = 1, PNG };
    /**
    * @brief ExportImageDialog
    * Constructor.
    * @param plot pointer to the plot to be exported.
    * @param mode export format.
    */
    ExportImageDialog(QCustomPlot * plot, ExportMode mode);
    /**
    * @brief ~ExportImageDialog
    * Destructor.
    */
    ~ExportImageDialog();

private:
    QCustomPlot *plot;
    int mode;
    /**
    * @brief ExportMode
    * Called by the constructor,
    * shows the dialog used to export the plot
    * depending on the choosed ExportMode.
    */
    void ExportImage();
};

#endif // EXPORTIMAGEDIALOG_H
