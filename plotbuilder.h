/*
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLOTBUILDER_H
#define PLOTBUILDER_H

#include <float.h>

#include "qcustomplot.h"
#include "modelwrapper.h"

#define ALL_LABEL "All"
#define DYNAMIC_PADDING_PARAMETER 900
#define PEN_WIDTH 3
#define PADDING_EVENT_VALUE DBL_MIN

class PlotBuilder
{
public:
    PlotBuilder();
    ~PlotBuilder();

    /**
    * @brief BuildPlotTypes
    * Construct the plot for the Types tab.
    * @param plotTypes pointer to the QCustomPlot object.
    * @param plotHeight the height of the plot.
    */
    void BuildPlotTypes(QCustomPlot * plotTypes, int plotHeight=0);

    /**
    * @brief BuildPlotModules
    * Construct the plot for the Modules tab.
    * @param plotModules pointer to the QCustomPlot object
    * @param plotHeight the height of the plot
    */
    void BuildPlotModules(QCustomPlot * plotModules, int plotHeight=0);

private:

    bbque::ModelWrapper *modelWrapper;
    int plotHeight;

};

#endif // PLOTBUILDER_H
