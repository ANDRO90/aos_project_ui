/*
 * Copyright (C) 2014  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "modelwrapper.h"
#include "ui_mainwindow.h"

namespace bbque {

// Global static pointer used to ensure a single instance of the class.
ModelWrapper *ModelWrapper::modelWrapperInstance = NULL;

ModelWrapper::ModelWrapper()
{
    static_model = new Model();
    dynamic_model_clean = new Model();
    dynamic_model_filtered_module = new Model();
    dynamic_model_filtered_type = new Model();
}

ModelWrapper::ModelWrapper(const ModelWrapper &modelWrapper)
{
    static_model = new Model();
    *static_model = *modelWrapper.static_model;

    dynamic_model_clean = new Model();
    *dynamic_model_clean = *modelWrapper.dynamic_model_clean;

    dynamic_model_filtered_module = new Model();
    *dynamic_model_filtered_module = *modelWrapper.dynamic_model_filtered_module;

    dynamic_model_filtered_type = new Model();
    *dynamic_model_filtered_type = *modelWrapper.dynamic_model_filtered_type;
}

ModelWrapper& ModelWrapper::operator=(const ModelWrapper& modelWrapper)
{
    static_model = modelWrapper.static_model;
    dynamic_model_clean = modelWrapper.dynamic_model_clean;
    dynamic_model_filtered_module = modelWrapper.dynamic_model_filtered_module;
    dynamic_model_filtered_type = modelWrapper.dynamic_model_filtered_type;

    return *this;
}

ModelWrapper::~ModelWrapper() {
}

ModelWrapper* ModelWrapper::GetInstance()
{
    if (!modelWrapperInstance)   // Only allow one instance to be generated.
       modelWrapperInstance = new ModelWrapper();

    return modelWrapperInstance;
}

void ModelWrapper::RefreshStaticModel(std::string path, std::string archive)
{
    EventManager *em = static_model->GetEventManagerInstance();
    em->SetPath(path);
    em->SetArchive(archive);

    static_model->RefreshModel();
    static_model->PopulateStatistics();
}

QStringList ModelWrapper::GetDistinctModules()
{
    return static_model->GetDistinctModules();
}

QStringList ModelWrapper::GetDistinctApplications()
{
    return static_model->GetDistinctApplications();
}

QStringList ModelWrapper::GetDistinctResources()
{
    return static_model->GetDistinctResources();
}

QStringList ModelWrapper::GetDistinctTypes()
{
    return static_model->GetDistinctTypes();
}

Model *ModelWrapper::GetStaticModel()
{
    return static_model;
}

void ModelWrapper::SetStaticModel(Model *model)
{
    static_model = model;
}

Model *ModelWrapper::GetDynamicModelClean()
{
    return dynamic_model_clean;
}

void ModelWrapper::SetDynamicModelClean(Model *model)
{
    *dynamic_model_clean = *model;
}

Model *ModelWrapper::GetDynamicModelFilteredModule()
{
    return dynamic_model_filtered_module;
}

void ModelWrapper::SetDynamicModelFilteredModule(Model *model)
{
    *dynamic_model_filtered_module = *model;
}

Model *ModelWrapper::GetDynamicModelFilteredType()
{
    return dynamic_model_filtered_type;
}

void ModelWrapper::SetDynamicModelFilteredType(Model *model)
{
    *dynamic_model_filtered_type = *model;
}

void ModelWrapper::SetDynamicModelFiltered(Model *to, Model *from)
{
    *to = *from;
}


void ModelWrapper::ApplyFilters(Model *model, QString m,
                              QString a, QString r, QString t,
                              QString from, QString to)
{
    SetDynamicModelFiltered(model, dynamic_model_clean);
    model->ApplyFilters(m, a, r, t, from, to);
}

} // namespace bbque
