/*
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <QAbstractTableModel>
#include <QStandardItemModel>
#include "bbque/em/event.h"
#include "bbque/em/event_wrapper.h"
#include "bbque/em/event_manager.h"

namespace bbque {

/**
    This class is the Model for the model-view pattern
*/
class Model : public QAbstractTableModel
{
    Q_OBJECT
public:

    /**
    * @brief Constructor
    */
    Model();

    /**
    * @brief Destructor
    */
    ~Model();

    /**
    * @brief Copy operator
    */
    Model& operator=(const Model&);

    /** @brief CloneItemModel
     * This function is called to create a clone of the ItemModel
    */
    void CloneItemModel(QStandardItemModel*, QStandardItemModel*);

    /**
    * @brief Get the list of all events
    */
    inline std::vector<Event> GetEvents()
    {
        return this->events;
    }

    /** @brief GetEventManagerInstance()
     * This function is called to create an instance of the class.
     * Calling the constructor publicly is not allowed. The constructor
     * is private and is only called by this Instance function.
    */
    static EventManager *GetEventManagerInstance();

    /**
    * @brief Get the number of modules
    */
    inline int GetModulesCount()
    {
        return modules_count;
    }

    /**
    * @brief Set the number of modules
    * @param count The new number of modules
    */
    void SetModelsCount(int count);

    /**
    * @brief GetEventsByModule
    * @param module The target module
    * @return Vector of pairs <timestamp, value> of all the
    * events of a specific module
    */
    std::vector<std::pair<double, int>> GetEventsByModule(QString module);

    /**
    * @brief GetEventsByType
    * @param type The target type
    * @return Vector of pairs <timestamp, value> of all the
    * events of a specific type
    */
    std::vector<std::pair<double, int>> GetEventsByType(QString type);

    /**
    * @brief GetAllEvents
    * @return Vector of pairs <timestamp, value> of all the events
    */
    std::vector<std::pair<double, int>> GetAllEvents();

    /**
    * @brief GetRangeValuesByModule
    * @param module The target module
    * @return Pair <min_value, max_value> specified in the settings dialog
    * for the specified module
    */
    std::pair<double, double> GetRangeValuesByModule(QString module);

    /**
    * @brief GetDistincts
    * @return List of all possible distinct names (used
    * for populating the filters)
    */
    QStringList GetDistinctModules();
    QStringList GetDistinctApplications();
    QStringList GetDistinctResources();
    QStringList GetDistinctTypes();

    /**
    * @brief GetPresents
    * @return List of all names present in the events (used
    * differentiating the plots)
    */
    QStringList GetPresentModules();
    QStringList GetPresentApplications();
    QStringList GetPresentResources();
    QStringList GetPresentTypes();

    /**
    * @brief SetPresent names
    */
    void SetPresentModules(QStringList);
    void SetPresentApplications(QStringList);
    void SetPresentResources(QStringList);
    void SetPresentTypes(QStringList);

    /**
    * @brief GetLowestTimestamp
    * @return Lowest timestamp among all the events
    */
    double GetLowestTimestamp();

    /**
    * @brief GetHighestTimestamp
    * @return Highest timestamp among all the events
    */
    double GetHighestTimestamp();

    void SetLowestTimestamp(double);
    void SetHighestTimestamp(double);

    /**
    * @brief GetLowestValue
    * @return Lowest value among all the events
    */
    int GetLowestValue();

    /**
    * @brief GetHighestValue
    * @return Highest value among all the events
    */
    int GetHighestValue();

    void SetHighestValue();
    void SetLowestValue();

    /**
    * @brief GetModelItems
    * @return List of the items of the model presented
    * in the TableView of the settings dialog for permitting users
    * to select the desired modules and relative value range
    */
    QList<QStandardItem*> GetModelItems();

    /**
    * @brief GetModulesModel
    * @return Entire modules model for the TableView of the settings
    * dialog
    */
    QStandardItemModel *GetModulesModel();

    void SetModulesModel(QStandardItemModel *model);

    /**
    * @brief GetSelectedModules
    * @return List of modules selected in the settings dialog
    */
    QStringList GetSelectedModules();

    void SetSelectedModules(QStringList);

    void RemoveSelectedModule(QString);
    void InsertSelectedModule(QString);

    /**
    * @brief RemoveUnselected
    * Remove from the list of events the ones not belonging to
    * a module selected in the settings dialog
    */
    void RemoveUnselected();

    /**
    * @brief RemoveItemFromSelectedModules
    * Remove the target module from the list of selected modules
    * @param module The name of the module which is to be
    * removed from the list of selected modules
    */
    void RemoveItemFromSelectedModules(std::string module);

    void SortSelectedModules();

    /**
    * @brief RefreshModel
    * Load the events
    * @brief SwitchArchiveCall of the new archive selected in the settings
    * dialog. (The target archive is already pointed to by a path
    * in the EventManager)
    */
    void RefreshModel();

    /**
    * @brief ApplyFilters
    * Updated the module according to the values specified by
    * each filter
    */
    void ApplyFilters(QString module, QString application,
                      QString resource, QString type, QString from,
                      QString to);

    /**
    * @brief SwitchArchiveCall
    * @brief PopulateStatistics
    * Update Higher and Lower Timestamps and values and
    * populate Distincts and Presents according to the events
    * present in the model
    */
    void PopulateStatistics();

    /**
    * @brief PopulateDistincts
    * Populate Distincts according to the events present in
    * the model
    */
    void PopulateDistincts();

    /**
    * @brief PopulatePresents
    * Populate Presents according to the events present in
    * the model at the
    * @brief SwitchArchiveCall time
    */
    void PopulatePresents();

    /**
    * @brief GetEventByTimestampAndType
    * @param timestamp The target timestamp
    * @param type The target type
    * @return Event corresponding to the specific timestamp and type
    */
    Event GetEventByTimestampAndType(QString timestamp, QString type);

    /**
    * @brief GetEventByTimestampAndModule
    * @param timestamp The target timestamp
    * @param module The target module
    * @return Event corresponding to the specific timestamp and module
    */
    Event GetEventByTimestampAndModule(QString timestamp, QString module);

    /**
    * @brief Set the list of events
    */
    void SetEvents(std::vector<Event> events);

    enum { ID=0, TIMESTAMP, MODULE, RESOURCE, APPLICATION, TYPE,
           VALUE, MAX_COLS };

    int rowCount(const QModelIndex &parent = QModelIndex()) const ;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role) const;

private:
    std::vector<Event> events;

    /**
    * @brief eventManagerInstance
    * Global static pointer used to ensure a single instance of the class.
    */
    static EventManager *eventManagerInstance;

    /**
    * @brief names of all the items related to the events after the
    * settings are specified. these lists remain unchanged when the
    * filters change. (used for populating the filters)
    */
    QStringList distinct_modules;
    QStringList distinct_applications;
    QStringList distinct_resources;
    QStringList distinct_types;

    QStringList selected_modules;

    /**
    * @brief modules_model
    * Model of the modules (and respective value range) chosen by
    * the user in the settings dialog
    */
    QStandardItemModel *modules_model;

    int modules_count;

    /**
    * @brief names of all the items related to the events present in the
    * model in accordance with the applied filters.
    * these lists change when the filters change. (used for understanding
    * which plots it is necessary to plot)
    */
    QStringList present_modules;
    QStringList present_applications;
    QStringList present_resources;
    QStringList present_types;

    double lowest_timestamp;
    double highest_timestamp;

    int lowest_value;
    int highest_value;
};

} // namespace bbque

#endif // MODEL_H_
