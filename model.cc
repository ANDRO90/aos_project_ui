#include "model.h"

namespace bbque
{

// Global static pointer used to ensure a single instance of the class.
EventManager *Model::eventManagerInstance = NULL;

Model::Model()
{
}

Model& Model::operator=(const Model& model)
{
    events = model.events;
    distinct_modules = model.distinct_modules;
    distinct_applications = model.distinct_applications;
    distinct_resources = model.distinct_resources;
    distinct_types = model.distinct_types;

    present_modules = model.present_modules;
    present_applications = model.present_applications;
    present_resources = model.present_resources;
    present_types = model.present_types;

    lowest_timestamp = model.lowest_timestamp;
    highest_timestamp = model.highest_timestamp;
    lowest_value = model.lowest_value;
    highest_value = model.highest_value;

    selected_modules = model.selected_modules;

    //clone the modules model, not just copy the pointer
    modules_model = new QStandardItemModel();
    CloneItemModel(model.modules_model, modules_model);

    modules_count = model.modules_count;

    return *this;
}

Model::~Model()
{

}

void Model::CloneItemModel(QStandardItemModel* from, QStandardItemModel* to)
{
    to->removeRows(0, to->rowCount ());
    for(int i=0; i<from->rowCount(); i++)
    {
        for(int j=0; j<from->columnCount(); j++ )
        {
            to->setItem(i, j, from->item(i,j)->clone());
        }
    }

    to->setHeaderData(0, Qt::Horizontal, tr("Module"));
    to->setHeaderData(1, Qt::Horizontal, tr("Range From"));
    to->setHeaderData(2, Qt::Horizontal, tr("Range To"));
}

EventManager* Model::GetEventManagerInstance()
{
    if (!eventManagerInstance)   // Only allow one instance to be generated.
       eventManagerInstance = new EventManager(true);

    return eventManagerInstance;
}

void Model::SetModelsCount(int count)
{
    modules_count = count;
}

std::pair<double, double> Model::GetRangeValuesByModule(QString module)
{
    std::pair<double, double> result = std::pair<double, double>(0.0,
                                                (double)GetHighestValue());

    for(int i = 0; i < GetModulesCount(); i++)
    {
        QStandardItem *m = modules_model->item(i, 0);

        if(m->text() == module)
        {
            result.first = modules_model->item(i, 1)->text().toDouble();
            result.second = modules_model->item(i, 2)->text().toDouble();

            break;
        }
    }

    return result;
}

void Model::SetEvents(std::vector<Event> events)
{
    this->events = events;
}

QStringList Model::GetDistinctModules()
{
    return distinct_modules;
}

QStringList Model::GetDistinctApplications()
{
    return distinct_applications;
}

QStringList Model::GetDistinctResources()
{
    return distinct_resources;
}

QStringList Model::GetDistinctTypes()
{
    return distinct_types;
}

QStringList Model::GetPresentModules()
{
    return present_modules;
}

QStringList Model::GetPresentApplications()
{
    return present_applications;
}

QStringList Model::GetPresentResources()
{
    return present_resources;
}

QStringList Model::GetPresentTypes()
{
    return present_types;
}

void Model::SetPresentModules(QStringList modules)
{
    present_modules = modules;
}

void Model::SetPresentApplications(QStringList applications)
{
    present_applications = applications;
}

void Model::SetPresentResources(QStringList resources)
{
    present_resources = resources;
}

void Model::SetPresentTypes(QStringList types)
{
    present_types = types;
}

QStringList Model::GetSelectedModules()
{
    return selected_modules;
}

void Model::SetSelectedModules(QStringList modules)
{
    this->selected_modules = modules;
}

void Model::RemoveSelectedModule(QString module)
{
    for(int i=0; i < selected_modules.size(); ++i)
    {
        if(selected_modules.at(i).contains(module))
            selected_modules.removeAt(i);
    }
}

void Model::InsertSelectedModule(QString module)
{
    if(!this->selected_modules.contains(module))
        this->selected_modules << module;
}

void Model::SortSelectedModules()
{
    selected_modules.sort();

    if(selected_modules.contains("All"))
    {
        int index = selected_modules.indexOf("All");
        if(index != 0)
            selected_modules.move(index, 0);
    }
}

QStandardItemModel *Model::GetModulesModel()
{
    return modules_model;
}

void Model::SetModulesModel(QStandardItemModel *model)
{
    model->setHeaderData(0, Qt::Horizontal, tr("Module"));
    model->setHeaderData(1, Qt::Horizontal, tr("Range From"));
    model->setHeaderData(2, Qt::Horizontal, tr("Range To"));
    modules_model = model;
}

QList<QStandardItem*> Model::GetModelItems()
{
    QList<QStandardItem*> result;

    for(int i = 0; i<modules_model->rowCount(); ++i)
    {
        result.push_back(modules_model->item(i, 0));
    }

    return result;
}

double Model::GetLowestTimestamp()
{
    return lowest_timestamp;
}

double Model::GetHighestTimestamp()
{
    return highest_timestamp;
}

void Model::SetLowestTimestamp(double timestamp)
{
    lowest_timestamp = timestamp;
}

void Model::SetHighestTimestamp(double timestamp)
{
    highest_timestamp = timestamp;
}

int Model::GetHighestValue()
{
    return highest_value;
}

int Model::GetLowestValue()
{
    return lowest_value;
}

void Model::SetHighestValue()
{
    int result = INT_MIN;

    for(std::vector<Event>::iterator it = events.begin();
        it != events.end(); it++)
    {
        if((*it).GetValue() > result)
        {
            result = (*it).GetValue();
        }
     }

    highest_value = result;
}

void Model::SetLowestValue()
{
    int result = INT_MAX;

    for(std::vector<Event>::iterator it = events.begin();
        it != events.end(); it++)
    {
        if((*it).GetValue() < result)
        {
            result = (*it).GetValue();
        }
     }

    lowest_value = result;
}

void Model::PopulateDistincts()
{
    QStringList d_modules;
    QStringList d_applications;
    QStringList d_resources;
    QStringList d_types;

    for(std::vector<Event>::iterator e = events.begin();
        e != events.end(); ++e)
    {
        d_modules.append(QString::fromStdString((*e).GetModule()));
        d_applications.append(QString::fromStdString((*e).GetApplication()));
        d_resources.append(QString::fromStdString((*e).GetResource()));
        d_types.append(QString::fromStdString((*e).GetType()));
    }

    d_modules.removeDuplicates();
    d_applications.removeDuplicates();
    d_resources.removeDuplicates();
    d_types.removeDuplicates();

    d_modules.sort();
    d_applications.sort();
    d_resources.sort();
    d_types.sort();

    d_modules.insert(0, "All");
    d_applications.insert(0, "All");
    d_resources.insert(0, "All");
    d_types.insert(0, "All");

    distinct_modules = d_modules;
    distinct_applications = d_applications;
    distinct_resources = d_resources;
    distinct_types = d_types;
}

void Model::PopulatePresents()
{
    QStringList p_modules;
    QStringList p_applications;
    QStringList p_resources;
    QStringList p_types;

    for(std::vector<Event>::iterator e = events.begin();
        e != events.end(); ++e)
    {
        p_modules.append(QString::fromStdString((*e).GetModule()));
        p_applications.append(QString::fromStdString((*e).GetApplication()));
        p_resources.append(QString::fromStdString((*e).GetResource()));
        p_types.append(QString::fromStdString((*e).GetType()));
    }

    p_modules.removeDuplicates();
    p_applications.removeDuplicates();
    p_resources.removeDuplicates();
    p_types.removeDuplicates();

    p_modules.sort();
    p_applications.sort();
    p_resources.sort();
    p_types.sort();

    p_modules.insert(0, "All");
    p_applications.insert(0, "All");
    p_resources.insert(0, "All");
    p_types.insert(0, "All");

    present_modules = p_modules;
    present_applications = p_applications;
    present_resources = p_resources;
    present_types = p_types;
}

void Model::RefreshModel()
{
    //em has already the update archive path
    bbque::EventManager *em = GetEventManagerInstance();

    bbque::EventWrapper ew;

    //each time it is necessary to deserialize all the events
    //of the archive and then re serialize them all, otherwise
    //the archive would be destroyed
    ew = em->Deserialize();
    em->Serialize(ew);

    //update the event list of the model
    SetEvents(ew.GetEvents());

    PopulateDistincts();
}

void Model::PopulateStatistics()
{
    PopulateDistincts();

    if(events.size() > 0)
    {
        lowest_timestamp =
                (double)events[events.size()-1].GetTimestamp().count();
        highest_timestamp =
                (double)events[0].GetTimestamp().count();
    }
    else
    {
        lowest_timestamp = 0;
        highest_timestamp = 0;
    }

    SetLowestValue();
    SetHighestValue();

    PopulatePresents();
}

void Model::ApplyFilters(QString module,
                         QString application, QString resource,
                         QString type, QString from, QString to)
{
    for(std::vector<Event>::iterator it = events.begin();
        it != events.end();)
    {
        std::string m = (*it).GetModule();
        std::string a = (*it).GetApplication();
        std::string r = (*it).GetResource();
        std::string t = (*it).GetType();

        QString timestamp = QString::number((*it).GetTimestamp().count());

        //remove each event not satisfying the constraints
        if((module != "All" && m != module.toStdString()) ||
                (application != "All" && a != application.toStdString()) ||
                (resource != "All" && r != resource.toStdString()) ||
                (type != "All" && t != type.toStdString()) ||
                (timestamp < from) || (timestamp > to))
        {
            events.erase(it);
        }
        else
            ++it;
     }

    PopulatePresents();
    PopulateStatistics();
}

void Model::RemoveUnselected()
{

    for(std::vector<Event>::iterator it = events.begin();
        it != events.end();)
    {
        bool found = false;

        for(int k = 0; k < selected_modules.size(); k++)
        {
            if((*it).GetModule() == selected_modules[k].toStdString())
            {
                found = true;
                break;
            }
        }

        if(!found)
        {
            events.erase(it);
        }
        else
            ++it;
     }
}


void Model::RemoveItemFromSelectedModules(std::string item)
{
    for(int k = 0; k < selected_modules.size(); k++)
    {
        if(selected_modules[k].toStdString() == item)
        {
            selected_modules.removeAt(k);
        }
    }
}

std::vector<std::pair<double, int>> Model::GetAllEvents()
{
    std::vector<std::pair<double, int>> result;

    for(std::vector<Event>::iterator it = events.begin();
        it != events.end(); it++)
    {
        std::pair<double, int> pair((double)(*it).GetTimestamp().count(),
                                    (*it).GetValue());
        result.push_back(pair);
     }

    return result;
}

std::vector<std::pair<double, int>> Model::GetEventsByType(QString type)
{
    std::vector<std::pair<double, int>> result;

    for(std::vector<Event>::iterator it = events.begin();
        it != events.end(); it++)
    {
        if((*it).GetType() == type.toStdString())
        {
            std::pair<double, int> pair((double)(*it).GetTimestamp().count(),
                                        (*it).GetValue());
            result.push_back(pair);
        }
     }

    return result;
}

std::vector<std::pair<double, int>> Model::GetEventsByModule(QString module)
{
    std::vector<std::pair<double, int>> result;

    for(std::vector<Event>::iterator it = events.begin();
        it != events.end(); it++)
    {
        if((*it).GetModule() == module.toStdString())
        {
            std::pair<double, int> pair((double)(*it).GetTimestamp().count(),
                                        (*it).GetValue());
            result.push_back(pair);
        }
     }

    return result;
}

int Model::rowCount(const QModelIndex & /*parent*/) const
{
    return events.size();
}

int Model::columnCount(const QModelIndex & /*parent*/) const
{
    return MAX_COLS;
}

QVariant Model::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();

    if((unsigned) index.row() >= events.size() || index.row() < 0)
        return QVariant();

    if(role == Qt::DisplayRole)
    {
        switch (index.column())
        {
            case ID:
                return index.row();

            case TIMESTAMP:{
                std::chrono::milliseconds timestamp =
                        events[index.row()].GetTimestamp();

                const int n = snprintf(NULL, 0, "%lu", timestamp.count());
                assert(n > 0);
                char buf[n+1];
                int c = snprintf(buf, n+1, "%lu", timestamp.count());
                assert(buf[n] == '\0');
                assert(c == n);

                return QString::fromStdString(buf);
            }
                break;

            case MODULE:
                return QString::fromStdString(
                            events[index.row()].GetModule());
                break;

            case APPLICATION:
                return QString::fromStdString(
                            events[index.row()].GetApplication());
                break;

            case RESOURCE:
                return QString::fromStdString(
                            events[index.row()].GetResource());
                break;

            case TYPE:
                return QString::fromStdString(
                            events[index.row()].GetType());
                break;

            case VALUE:
                return events.at(index.row()).GetValue();
                break;

            default:
                return QVariant();
                break;
        }
    }
    else
        return QVariant();
}

QVariant Model::headerData(int section,
                           Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
    {
        if(orientation == Qt::Horizontal)
        {
            switch (section)
            {
                case ID:
                    return QString( "#" );

                case TIMESTAMP:
                    return QString( "Timestamp" );

                case MODULE:
                    return QString( "Module" );

                case APPLICATION:
                    return QString( "Application" );

                case RESOURCE:
                    return QString( "Resource" );

                case TYPE:
                    return QString( "Type" );

                case VALUE:
                    return QString( "Value" );

                default:
                    return QString("Name not provided for this column");
            }
        }
    }
    return QVariant();
}

Event Model::GetEventByTimestampAndType(QString timestamp, QString type)
{
    for(std::vector<Event>::iterator it = events.begin();
        it != events.end(); it++)
    {      
        if((QString::number((*it).GetTimestamp().count()) == timestamp) &&
                ((*it).GetType() == type.toStdString()))
        {
            Event event(true, (*it).GetModule(), (*it).GetResource(),
                (*it).GetApplication(), (*it).GetType(), (*it).GetValue());

            event.SetTimestamp((*it).GetTimestamp());
            return event;
        }
     }

    //if not found return invalid event
    Event result(false, "", "", "", "", -1);
    return result;
}

Event Model::GetEventByTimestampAndModule(QString timestamp, QString module)
{
    for(std::vector<Event>::iterator it = events.begin();
        it != events.end(); it++)
    {

        if((QString::number((*it).GetTimestamp().count()) == timestamp) &&
                ((*it).GetModule() == module.toStdString()))
        {
            Event event(true, (*it).GetModule(), (*it).GetResource(),
                        (*it).GetApplication(), (*it).GetType(),
                        (*it).GetValue());
            event.SetTimestamp((*it).GetTimestamp());
            return event;
        }
     }

    //if not found return invalid event
    Event result(false, "", "", "", "", -1);
    return result;
}

}
